# Changelog

### [1.0.2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/compare/release/1.0.1...release/1.0.2) (2023-02-02)


### Bug Fixes

* **socle:** restore keycloak database without postgres admin user ([9df4d8c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/9df4d8c882b6f1cb10ca3045f510a4944dcb055f))

### [1.0.1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/compare/release/1.0.0...release/1.0.1) (2023-02-01)


### Bug Fixes

* **socle:** add postgres admin dbname for keycloak restore job ([6fbe840](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/6fbe8402b807426695694fa2fca43a2492b9a711))
* **socle:** create backup directory if not exists ([9f01139](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/9f01139f75debe53ce5af4b7583b6a498cf9d7f4))
* **socle:** use env vars instead of pg_dump options ([ad11b77](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/ad11b775ffc2dcc5aea162d1ae225445a75c3bcf))

## 1.0.0 (2023-02-01)


### Features

* add drawio service to addon ([05e071e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/05e071e94c95563d70343d240eb1aa5d0fbb4ec7))
* add screego addon ([0b1d94c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/0b1d94c89b9942b5ba46f4bd258fefa7376cbfc9))
* add service filepizza ([0e2d557](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/0e2d557a1ceea5df783f3755e4681f7556a782b6))
* **addons:** factorize init-keycloak of addons ([728670d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/728670dfc94a1d6822b91e88f05cec23ea2053de))
* **admin tools:** add command to generate admin tools installation files ([8cac909](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/8cac909b3dd06f503571a811cd4717aed7d38845))
* **admin tools:** move supercrud from addons to admin tools ([e6261f6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/e6261f6ecd6aaeb5107f42a0c39df8c49bd2bafc))
* **admin-toolt:** add admin tool documentation ([8b675bf](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/8b675bfdc4b0f1c5b07696f71a7041baa41bbc25))
* **agenda:** use registry defined in vars.ini ([137ed06](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/137ed06799fbfd399c8b98fc784763ebb3c6d11e))
* **blog:** use registry defined in vars.ini ([894aad2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/894aad259e0e6ce19f8417a6d6f98eb46f743614))
* **build:** add setuptools integration ([13fc97f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/13fc97fd0f0b43590b740568ebf650b81c57bbde))
* **build:** don't create jinja loader and environment per templates ([4b578e1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/4b578e1c7111f5ed2bff4b0e9712bacd7e79dc77))
* **build:** jinja includes must always find `include` directories ([93e2ea0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/93e2ea0197bf3fd34921b3a24618641db544bcd1))
* cert-manager http01 ingress solver can be used ([88b6ef3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/88b6ef30cbeabc15c26baa3a2531d23da42b04fe))
* deploy stable version ([108c014](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/108c0148291e71ee77890b10fbaa181954721f17))
* **deploy:** create a new empty realm if `realmImport` is `false` ([4ff69a3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/4ff69a3f70ca3b5257f62a73779b649d5de475b8))
* **deploy:** generate keycloak configuration secret only for import ([b868ae0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/b868ae05785d0f66acea692b0b9d3011dcd03577))
* **deploy:** initialise keycloak before installing `laboite` ([3cff8b4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/3cff8b44cc9b8d6212e17e9781c795dc0f150ba2))
* disable telemetry by default ([7ce1f9c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/7ce1f9c74a324399aaef01cf993230dbbc055d9e))
* finalize wikijs installation ([34ec01f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/34ec01fc1e1a184b7002b859457de3332c4d1e90))
* **frontal-nextcloud:** use registry defined in vars.ini ([142d69e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/142d69eddf604b7fa1f6816ad90557ace3c884ca))
* **gen-socle:** add possibility to not install keycloak or laboite ([722a932](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/722a9324c98e9885c01a5ce2771097e260c54798))
* **grafana:** add grafana admin tool ([2f93432](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/2f93432eb87304a304bdb76a940ea4ae5d84907c))
* **ingress-nginx:** upgrade to helm version 4.2.5 ([0b3c347](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/0b3c347bd592105d43452cfef734bb70a0b2196e))
* **ingress-nginx:** user can force `loadBalancerIP` value ([c205120](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/c20512033406630c1cdb20a512803ff89762af01))
* **keycloak:** import values are enabled only when configured ([235d179](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/235d179cbc738712d47b56332aa041b046cd2139))
* **keycloak:** use registry defined in vars.ini ([bc0f9f4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/bc0f9f40d75d3e164709c97fbcdee082794a0c48))
* **kubernetes-dashboard:** add a service account token for viewer ([e76a3b1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/e76a3b12c4cab11a46aebe55e3f96d621ffb5dfd))
* **kubernetes-dashboard:** add admin account ([d2534cd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/d2534cdb51869dcfa23db78e6febc8e31283047b))
* **kubernetes-dashboard:** add ingress annotations and hosts ([6822958](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/682295888ce0b411906c6342047089274efa282c))
* **kubernetes-dashboard:** add kubernetes dashboard admin tool ([b530945](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/b53094591e598fdebcec660846bd9a122e6c9b1f))
* **kubernetes-dashboard:** add undeploy script ([a043ff8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/a043ff8f6c20f47504fbb00e3f84e9e4ae82b9fb))
* **kubernetes-dashboard:** add viewer ([4c2e201](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/4c2e2012cdbfc69d7cde630c5ccd75d19ab01521))
* **kubernetes-dashboard:** configure resource requests and limits ([c2f11bd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/c2f11bd7e01c8081184408b2d1e56c4e0d16c193))
* **laboite:** add nexcloud parameters in meteor settings ([9db924d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/9db924ddcbd7cdc90f07b437c25ef48f78959e19))
* **laboite:** use registry defined in vars.ini ([9daab69](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/9daab69732315302d5cac3d8d6ecc11be46fb6df))
* **loki:** add loki admin tool ([ecaea01](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/ecaea0101cf8aa683c6f6ff726cd658d84cbda4d))
* **loki:** use the ingress ([6e581ba](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/6e581bab9b03dd9c4b86afe41e6673dd12ba1c33))
* **lookup-server:** use registry defined in vars.ini ([8a26d16](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/8a26d16f0b7ac3b858575bd4430f34b2b4839a64))
* **mezig:** use registry defined in vars.ini ([3032e40](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/3032e401736d3bed9b5c7ad0df4105f44f0d0f69))
* **mongodb:** Enable Prometheus metrics exporter ([4134090](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/41340902c8975883d22f94095a923297863e1fcc))
* permit to choose cluster-issuer name and certificate type ([bebfe4c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/bebfe4c72b46bd7205446871313b9f98c94f13ba))
* permit to use already installed cert-manager ([a565e8e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/a565e8ec70e7c3a420160cec4af430c707176aeb))
* **prometheus:** Add prometheus admin tool ([26395fc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/26395fc099d2788561dd30c9762e4374766f91f0))
* **promtail:** add promtail admin tool ([252863f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/252863fa720fceb4c57a979219acc53d2f9dace6))
* **promtail:** customize client URL (loki) ([7b399e1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/7b399e143c0ca0a724e0dde9331fdd9128b558df))
* **radicale:** use registry defined in vars.ini ([b8b6864](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/b8b68640706958c4e8c51a66fe4d78c1ea86af1a))
* **readme:** add latest addons ([5f85f42](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/5f85f4272592d91d2b4269b4f051e47f7a0c3d9f))
* rocketchat can be installed in its own namespace ([d955d14](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/d955d14f03a2fa189796fe4192faaa823e9b8b20))
* rocketchat does not show wizard on first run ([16d4d94](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/16d4d947dbf2e649886bbfe70e51a6fd9a0c660b))
* **socle:** build redirecturis for keycloak sso client ([cbd3fcd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/cbd3fcdaadb789c30e333cb6efa2cc02e3f74bdf))
* **socle:** correct and improve redirecturis generation ([3c05c75](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/3c05c75a9380b5db9d466014811c623b666bf31d))
* **socle:** gen-socle generates scripts to deploy 1.2.0 laboite chart ([4c82322](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/4c823227b42489bb839d25914801107cd9b86062))
* **socle:** implement keycloak backup and restore procedure ([2573700](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/2573700623b83575a0ac816dae262a0a7056264e))
* **socle:** implement mongodb backup and restore procedure ([31c0236](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/31c023687001939a1a6f915dc1f171ce4f4b966b))
* **sondage:** use registry defined in vars.ini ([38dff93](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/38dff935aa66f2be6fe6aa7d899980701620cf96))
* update supercrud admin tool ([bdcb7c4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/bdcb7c4fd6601e3dfc750ca1ea82478e10af831a))
* upgrade minio version ([978409b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/978409b7190d246cacdf7d8f54dbaba9644a53da))
* upgrade mongodb chart and app version ([630643b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/630643b2f102594072547e15692cfcd7dc9b73c7))
* **vars.ini:** define default registry ([2c59fbf](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/2c59fbff1175f55775351d81c6596453685654f8))
* **vars.ini:** import realm by default ([bd60cf6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/bd60cf68841f182bdf8cd80f45508e3c9b0cab6c))


### Bug Fixes

* add missing parameters for blog and blogapi ([4c4f80d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/4c4f80d6871ee95fab3e39aa665dc3f66cc1707b))
* **addon:** add chart-version in build and test scripts ([adca9a9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/adca9a98c0119df76e99adb5daf7fc2d139690fc))
* **addon:** add chart-version to excalidraw addon scripts ([d600a36](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/d600a369603032f158ae54fba5cd664b9de728d9))
* **addon:** add chart-version to wikijs addon deploy and test scripts ([a5466fb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/a5466fbc510e0476a080765d64a8c5c8345b22c2))
* **addon:** add demo mode for gitea addon ([475f810](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/475f810ab971b07bc2a47c529deb5d128f15d856))
* **addon:** gen-addon command can be execute outside tools directory ([1e69641](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/1e69641a2a1912149ff47d63ee9667d327835cff))
* **addon:** set correct helm repo for filepizza ([124c5d4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/124c5d47926f1e07e105e537a3b274b426c052a8))
* **addon:** set filepizza chart version ([0cb8e02](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/0cb8e0275385fade4d414df5b93b188fad26acfc))
* **admin-tool:** fix loki chart-version ([480a881](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/480a881f1cb9fa7bc51ce5993348c9faa583a773))
* **admin-tool:** promtail add chart-version to scripts ([f6c3f09](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/f6c3f0916b296425fb39574056f1c01b80456420))
* **admin-tool:** set correct dns service name for loki ([78b4ec5](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/78b4ec5aa1ee9bb090e24e492d3bd22e93a00cdf))
* **admin-tools:** internal minio for loki supports demomode ([e922b8c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/e922b8c945c2eb5343072f27125f45993022a031))
* **build:** add laboite parameters ([0a35ede](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/0a35edec7ec767a0287f83f6ef6ad74fb1fe1f55))
* **build:** display correct output directory in readme ([7a9b067](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/7a9b067c893143aa470a7f2b35c57bf8b4e641be))
* **build:** does not run outside of the `tools/` directory ([7503985](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/7503985d5c783330e29c152ff86974d500566796))
* codimd can be installed in its own namespace ([f5c04cb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/f5c04cbc86d9441c04a982fe71da6dacccdaff69))
* **deploy:** use nginx's chart name in its release name ([05b97eb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/05b97ebe3df58f181a04968dc2b803622c62abbf))
* **filepizza:** add chart version to filepizza ([1a77eb1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/1a77eb1624a5125db95668209026144f711afcab))
* ignore make minio bucket if already exists ([fae7104](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/fae7104658908c1f7e33df6818a7ca6204cd0569))
* **ingress-nginx:** upgrade chart version to 4.2.5 ([3c5be04](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/3c5be04a77e31afcb344c871deed01e5958f2cd2))
* installation don't fail if users already present ([f045101](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/f045101495efc766a73eef0d4198da5320e4d465))
* **keycloak:** cleaning keycloak values template ([c5a7b84](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/c5a7b84fba0238e15651cbd7fbddccb53b12f411))
* **keycloak:** jinja includes are relative to `templates` directory ([bca6181](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/bca6181472588eee03b0c4b2cf1c7c78cf44b7c1))
* **kubernetes-dashboard:** prevent mixup beween go template and jinja ([8fd43ab](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/8fd43ab26ccadf9351f0149a5fefa96e2a8d5e70))
* **loki:** disable authentication ([3cb45fc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/3cb45fc2bc9be78426a2df1df48072a3a062bcac))
* **loki:** use local minio ([ea077ad](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/ea077ade6b0440687727d6b14fa47d374430f0fc))
* **minio:** cleaning minio values ([6210ce2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/6210ce2e64dab93f8104e498ca7fd4330ad9635f))
* **mongodb:** update chart version and clean values file ([e63cef8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/e63cef80bd2a28b8327e7cf88b0df43c2171a53f))
* parsing configuration file fails when a percent is present ([a46cb19](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/a46cb1949e2e7d456e8fcdad875a70884ff54520))
* **prometheus:** add missing helm repo ([c9bbcff](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/c9bbcffccccea0e1b94c78bcc4762fba52c4114f))
* **prometheus:** missing helm repository ([afd72e1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/afd72e1108334b11883b0ef7a6e3dd1eaa156067))
* **radicale:** add missing parameters ([9215acd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/9215acd175785d9c1ba892f34d658c4557c923be))
* removed unused boolean var for resources ([48435b4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/48435b4c1e7cae63de91835f2944e98f5aae7e81))
* restore vars.ini before bad conflict resolution ([e6e3d6c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/e6e3d6c8cc68ab99b4268a27766fd1040f129a37))
* **socle:** add replicacount parameters for all socle services ([685d0d7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/685d0d79fcff5fe9421acb7c5fc02ba69a319c07))
* **socle:** configure demo mode for minio ([59e93f6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/59e93f69f762ef5635cb1cd13410eab55ba22eda))
* **socle:** fix metrics tests in keycloak values template ([dd9d7da](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/dd9d7da68bda1a119d8889cf52784ff788c0e0b0))
* **socle:** generate radicale keycloak client creation ([486da0b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/486da0b3c286e037d4dba79d1f9ac3868a11e39d))
* **socle:** include keycloak metrics events only if enabled ([2c7e5de](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/2c7e5de3e8df3b11d6d4acf394738944ad8f78e3))
* **socle:** init-keycloak includes only one file at a time ([90943fc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/90943fcbbfcae9b911842f2e3c1ed47c18b860b8))
* **socle:** testing tools version must deploy testing charts versions ([a74f0e2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/a74f0e221256338e605d83e13252031fb21329bb))
* update ingress configuration. ([82198ad](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/82198adb0278024087d513c8fbd6362980086e39))
* update kubernetes-dashboard ingress for cert-manager ([3099043](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/3099043c0257f0f5c1df8596774782c79da29bc2))
* update loki ingress for cert-manager ([197915d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/197915d16c084023f2cf69790f8b645412aad386))
* update mongodb grafana dashboard ([44149fb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/44149fbfc4fef3a537cc869e70a99cc99dfd346f))
* update prometheus-stack ingress for cert-manager ([80c44e6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/80c44e619d12996040785ecb869a27680f13a320))
* **utils-log.sh.j2:** jinja includes are relative to `templates/` ([61ec0df](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/61ec0dfcfa33976501a841b9cfdeba33d81e04d9))
* **values:** add nextcloud values for user, password and quota ([8e355ea](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/8e355ea7b9b7c78cecd1ebebc5de5a47df6a52be))


### Code Refactoring

* **build:** template generation is always the same logic ([5d68b9b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/5d68b9be789c889aca5696f2f34dac4d6ddec944))


### Styles

* **build:** one import per line from generic to specific ([3ec7579](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/3ec75796ddde82d678f2fc4d8ad67bbf92e374c3))
* **build:** reformat and add missing function documentation ([75f234e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/75f234e4550900f1dfced4466ea173d43cde8db7))
* **build:** reformat with python black ([ad5ded0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/ad5ded00249eeff5013b20c6527aa152b9359775))
* **build:** use f-strings instead of `.format()` ([2ddaa1c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/2ddaa1ca137772cffab9747d8d62db3e9fb289ad))
* **include:** relative path is useless ([e7e9efc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/e7e9efc0a113048d011a4a76d197afdb04b05f76))


### Documentation

* **contributing:** explain commit message formatting ([c417391](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/c4173915924e4f0af99d23ac5667fd16565eea35))


### Continuous Integration

* **addon:** add filepizza addon ([09c42ee](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/09c42ee5ca74f4657554d95c354334421113e7f5))
* **build:** generate all addons ([b11ead1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/b11ead161732db9b3fcd218d2219407a22213376))
* **build:** generate all admin tools ([800f3f4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/800f3f41d93103e22b0bdae66213ad948ee7a279))
* **build:** generate socle ([07ce1fd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/07ce1fd58fc1db80c1c6cb9683588b01d6c9d230))
* **chart:** test socle charts templates ([e6b7c0f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/e6b7c0f82979c10fb3127cd7c15f3d8d7a14b2af))
* **helm-template:** test addon chart template ([81f7398](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/81f73985d0acb25e610615a650cb0c043d10e2a1))
* **helm-template:** test admin-tools chart template ([99e2676](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/99e2676b1b7b0961e1b409422aafa91f323efa2f))
* **initial-checks:** enforce git commit message formatting ([88e8715](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/88e8715370b71c61f01dac90027a6902f4ead2e1))
* **lint:** enforce python black formatting of build script ([3b6b651](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/3b6b651d3d0f087912b2b91222d5e625c12fdddc))
* **release:** avoid regression in `dev` branch ([e30036a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/e30036ad0321ca144c168b56fd9f2a6fa06ca399))
* **release:** create `testing` and `stable` releases ([20c3ba8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/commit/20c3ba8b7c33b5f36dab3bf3e7c120e57023f694))
