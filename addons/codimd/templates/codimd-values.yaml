# Default values for codimd.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

# storageClass:
# nameOverride:
# fullnameOverride:

image:
  # When you use export pdf with CJK character, please change the tag with postfix `-cjk`.
  # for example 2.2.0-cjk
  {%- if config['codimd']['tag'] %}
  tag: "{{ config['codimd']['tag'] }}"
  {%- endif %}
  pullPolicy: IfNotPresent
  pullSecrets: []
#    - myRegistryKeySecretName

##
## PostgreSQL chart configuration
##
## Reference: https://github.com/bitnami/charts/blob/master/bitnami/postgresql/values.yaml
##
## If you want to use external database, just set postgresql.enabled to false
##
postgresql:
  {%- if config['codimd']['dbprovider'] == "postgres" and config['codimd']['dbaddress'] == "internal" %}
  enabled: true
  volumePermissions:
    enabled: true
  postgresqlUsername: {{ config['codimd']['dbusername'] }}
  postgresqlPassword: {{ config['codimd']['dbpassword'] }}
  postgresqlDatabase: {{ config['codimd']['dbname'] }}
  {%- else %}
  enabled: false 
  {%- endif %}
##
## MariaDB chart configuration
##
## Reference: https://github.com/bitnami/charts/blob/master/bitnami/mariadb/values.yaml
##
## If you prefer MariaDB, we also supported.
##
mariadb:
  {%- if config['codimd']['dbprovider'] == "mariadb" %}
  enabled: true
  volumePermissions:
    enabled: true
  db:
    user: {{ config['codimd']['dbusername'] }}
    password: {{ config['codimd']['dbpassword'] }}
    name: {{ config['codimd']['dbname'] }}
  master:
    persistence:
      enabled: true
  replication:
    enabled: false
  {%- else %}
  enabled: false
  {%- endif %}

##
## Kubernetes service
## use NodePort for minikube
## other environment use LoadBalancer or ClusterIP
##
service:
  annotations: {}
  type: "ClusterIP"
  port: 80 
  # externalTrafficPolicy:
  # loadBalancerIP:

##
## Ingress configuration
##
ingress:
  enabled: true
  {%- if config['general']['ingressController'] == "nginx" %} 
  className: {{ config['general']['ingressClassName'] | default('nginx', true) }}
  annotations:
    nginx.ingress.kubernetes.io/upstream-hash-by: "$uri$arg_noteid"
  {%- if config['cert-manager']['enabled'] == "true" %}
    cert-manager.io/cluster-issuer: "{{ config['general']['ingressClusterIssuerName']|default('letsencrypt', true) }}-{{ config['cert-manager']['type'] }}"
  {%- endif %}
  {%- else %}
  annotations: {}
  {%- endif %}
  # Defines hosts to add to Traefik routes
  hosts:
  - host: {{ config['codimd']['hostname'] }}.{{ config['general']['domain'] }}
    paths:
    - path: /
      pathType: ImplementationSpecific
  # Use TLS for these FQDN
  tls:
  - hosts:
    - {{ config['codimd']['hostname'] }}.{{ config['general']['domain'] }}
    {%- if config['cert-manager']['enabled'] == "true" %}
    secretName: tls-{{ config['codimd']['hostname'] }}
    {%- endif %}
##
## CodiMD application configuration
##
codimd:
  ##
  ## Affinity for pod assignment
  ## Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
  ##
  affinity: {}
  ##
  ## Tolerations for pod assignment. Evaluated as a template.
  ## ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
  ##
  tolerations: {}
  ##
  ## Node labels for pod assignment. Evaluated as a template.
  ## ref: https://kubernetes.io/docs/user-guide/node-selection/
  ##
  nodeSelector: {}
  ##
  ## Pod annotations
  ## ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/
  ##
  podAnnotations: {}
  ##
  ## security context
  ##g
  securityContext:
    runAsGroup: 1500
    runAsUser: 1500
    fsGroup: 1500
    runAsNonRoot: true

  ##
  ## connection setting
  ##
  connection:
    ## if you doesn't using ingress, you can setup domain for your CodiMD instances
    # domain:
    ## if you want to add port number on your url
    urlAddPort: false
    ## if you use load balancer or setup TLS on ingress
    protocolUseSSL: false

  {%- if config['codimd']['dbprovider'] == "postgres" and config['codimd']['dbaddress'] != "internal" %}
  ##
  ## External database configuration
  ##   if you want to use external database
  ##   database type only support mysql and postgres
  ##
  database:
    type: postgres
    host: {{ config['codimd']['dbaddress'] }}
    port: {{ config['codimd']['dbport'] }}
    username: {{ config['codimd']['dbusername'] }}
    password: {{ config['codimd']['dbpassword'] }}
    databaseName: {{ config['codimd']['dbname'] }}
  {% endif %}

  ##
  ## Image upload store
  ##
  imageUpload:
    storeType: minio
  imageStorePersistentVolume:
    enabled: false

  ## for advanced used, manually setup environment for used
  extraEnvironmentVariables:
    # CMD_LOGLEVEL: info
    CMD_MINIO_ACCESS_KEY: {{ config['minio']['minioAccess'] }}
    CMD_MINIO_SECRET_KEY: {{ config['minio']['minioSecret'] }}
    {%- if config['minio']['minioEndPoint'] != 'internal' %}
    CMD_MINIO_ENDPOINT: {{ config['minio']['minioEndPoint'] }}
    {%- else %}
    CMD_MINIO_ENDPOINT: minio.{{ config['general']['domain'] }}
    {%- endif %}
    CMD_MINIO_PORT: 443
    CMD_MINIO_SECURE: true
    CMD_S3_BUCKET: {{ config['minio']['minioBucket'] }}

  ## automatically check new version
  versionCheck: true

  ##
  ## about security setting
  ##
  security:
    useCDN: 'false'
    # session secret, please change this value
    sessionSecret: 'changeit'
    sessionLife: '1209600000'
    ## HSTS setting
    hstsEnabled: 'true'
    hstsMaxAge: '31536000'
    hstsIncludeSubdomain: 'false'
    hstsPreload: 'true'
    ## CSP setting
    cspEnabled: 'true'
    # cspReportUri:
    ## setup allow origin
    # allowOrigin:
    ## use gravatar.com as user gravatar
    allowGravatar: 'true'

  ##
  ## for tooBusy block user when server event loop lag more than ? ms
  ##
  responseMaxLag: '70'

  ##
  ## setting about note creation
  ##
  noteCreation:
    freeUrlEnabled: 'false'
    freeUrlForbiddenNoteIds: 'robots.txt,favicon.ico,api'
    defaultPermission: 'editable'
  ##
  ## setting about note permission
  ##
  notePermission:
    allowAnonymousEdit: true
    allowAnonymousView: true
  ##
  ## allow export note to pdf
  ##
  allowPDFExport: false
  ##
  ## setting about markdown
  ##
  markdown:
    # plantUMLServer:
    useHardBreak: true
    linkifyHeaderStyle: 'keep-case'

  ##
  ## User Authentication Methods
  ##
  authentication:
    ##
    ## authentication by Email and Password
    local:
      enabled: false
      allowRegister: false
    bitbucket:
      enabled: false
      key:
      secret:
    dropbox:
      enabled: false
      appKey:
      appSecret:
    facebook:
      enabled: false
      clientId:
      secret:
    github:
      enabled: false
      clientId:
      secret:
      ## provide enterprise url if you use GitHub Enterprise Version
      enterpriseUrl:
    gitlab:
      enabled: false
      domain:
      scope:
      applicationId:
      secret:
    google:
      enabled: false
      clientId:
      secret:
      hostedDomain:
    ldap:
      enabled: false
      providerName:
      url:
      tlsCA:
      bindDN:
      bindCredentials:
      searchBase:
      searchFilter:
      searchAttributes:
      attributes:
        id:
        username:
    mattermost:
      enabled: false
      domain:
      clientId:
      secret:
    oauth2:
      enabled: true
      providerName: {{ config['keycloak']['realm'] }}
      domain: {{ config['keycloak']['hostname'] }}.{{ config['general']['domain'] }}
      clientId: {{ config['keycloak']['clientName'] }}
      secret: {{ config['keycloak']['clientSecret'] }}
      authorizationUrl: https://{{ config['keycloak']['hostname'] }}.{{ config['general']['domain'] }}/auth/realms/{{ config['keycloak']['realm'] }}/protocol/openid-connect/auth
      tokenUrl: https://{{ config['keycloak']['hostname'] }}.{{ config['general']['domain'] }}/auth/realms/{{ config['keycloak']['realm'] }}/protocol/openid-connect/token
      userProfileUrl: https://{{ config['keycloak']['hostname'] }}.{{ config['general']['domain'] }}/auth/realms/{{ config['keycloak']['realm'] }}/protocol/openid-connect/userinfo
      scope: email
      attributes:
        username: preferred_username
        displayName: name
        email: email
    openID:
      enabled: false
    saml:
      enabled: false
      idpSSOUrl:
      idpCert:
      issuer:
      identifierFormat:
      disableRequestedAuthnContext:
      groupAttribute:
      externalGroups:
      requiredGroups:
      attributes:
        id:
        username:
        email:
    twitter:
      enabled:
      consumerKey:
      comsumerSecret:

# Autoscaling configuration
autoscaling:
  enabled: {{ config['codimd']['autoscalingEnabled']|default(config['general']['autoscalingEnabled'], true) }}
  # The minimum and maximum number of replicas for the Agenda Deployment
  minReplicas: {{ config['codimd']['autoscalingMinReplicas'] | default(config['general']['autoscalingMinReplicas'], true) }}
  maxReplicas: {{ config['codimd']['autoscalingMaxReplicas'] | default(config['general']['autoscalingMaxReplicas'], true) }}
  # The metrics to use for scaling
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: {{ config['codimd']['cpuAverageUtilization'] | default(config['general']['cpuAverageUtilization'], true) }}
  behavior:
    scaleDown:
      stabilizationWindowSeconds: 300
      policies:
      - type: Pods
        value: 1
        periodSeconds: 300
# Requested resources for a pod. Required for autoscaling
resources:
  requests:
    cpu: "{{ config['codimd']['requests-cpu'] | default(config['general']['requests-cpu'], true) }}"
    memory: "{{ config['codimd']['requests-memory'] | default(config['general']['requests-memory'], true) }}"
  limits:
    cpu: "{{ config['codimd']['limits-cpu'] | default(config['general']['limits-cpu'], true) }}"
    memory: "{{ config['codimd']['limits-memory'] | default(config['general']['limits-memory'], true) }}"
