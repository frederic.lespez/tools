#!/bin/bash

{% include 'include/utils-log.sh.j2' %}

# Check all mandatory values are set
if [ -z "{{ config['mastodon']['secret_key_base'] }}" ] ||
   [ -z "{{ config['mastodon']['otp_secret'] }}" ] ||
   [ -z "{{ config['mastodon']['vapid_private_key'] }}" ] ||
   [ -z "{{ config['mastodon']['vapid_public_key'] }}" ] ||
   [ -z "{{ config['mastodon']['postgrePassword'] }}" ] ||
   [ -z "{{ config['mastodon']['redisPassword'] }}" ] ||
   [ -z "{{ config['mastodon']['minioBucket'] }}" ]
then
  log 'Error: one mandatory value is empty/unset!'
  exit 1
fi

log "Prepare minio"
kubectl create namespace {{ config['general']['namespace'] }}
kubectl apply -n {{ config['general']['namespace'] }} -f minio-policy.yaml
kubectl apply -n {{ config['general']['namespace'] }} -f minio-job.yaml
kubectl wait -n {{ config['general']['namespace'] }} --for=condition=Complete --timeout=600s job/init-minio

{% if config['keycloak']['enabled'] == "true" -%}
#Prepare keycloak
log "Prepare keycloak"
bash init-keycloak
{%- endif %}

# Clone the mastodon helm's chart repository. There's no helm chart repo provided by mastodon for now,
# but they still provide a helm chart.
log "Git clone Helm chart"
git clone --single-branch --depth 1 https://github.com/mastodon/chart.git mastodon-git
log "Update Helm dependancies"
cd mastodon-git
helm dep update
cd ..

# Install mastodon
log "Install Mastodon"
helm upgrade -i --create-namespace -n {{ config['general']['namespace'] }} mastodon mastodon-git/ -f mastodon-values.yaml

# Restart all mastodon services because of a bug. This is a workaround.
# If you encounter some backend error, like "an error page that says something went wrong",
# restart all services like in the line bellow
log "Restart all mastodon services"
# TODO: find the right condition to wait for, before restarting services
kubectl rollout -n {{ config['general']['namespace'] }} restart deployment mastodon-web mastodon-sidekiq-all-queues mastodon-streaming

log "Wait for MinIO job to finish"
kubectl wait -n {{ config['general']['namespace'] }} --for=condition=complete --timeout=600s job/init-minio
log "Remove MinIO job and policy"
kubectl delete -n {{ config['general']['namespace'] }} -f minio-policy.yaml
kubectl delete -n {{ config['general']['namespace'] }} -f minio-job.yaml
