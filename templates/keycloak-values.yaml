# The number of replicas to create (has no effect if autoscaling enabled)
replicas: {{ config['keycloak']['autoscalingMinReplicas'] | default(config['general']['autoscalingMinReplicas'], true) }}

image:
  # The Keycloak image repository
  repository: {{ config['general']['registry'] }}/apps/sso
  #repository: docker.io/jboss/keycloak
  # Overrides the Keycloak image tag whose default is the chart version
  tag: "{{ config['keycloak']['tag'] }}"
  # The Keycloak image pull policy
  pullPolicy: Always

rbac:
  create: true
  rules:
  # RBAC rules for KUBE_PING
    - apiGroups:
        - ""
      resources:
        - pods
      verbs:
        - get
        - list

# Additional environment variables for Keycloak
extraEnv: |
  # - name: KEYCLOAK_LOGLEVEL
  #   value: DEBUG
  # - name: WILDFLY_LOGLEVEL
  #   value: DEBUG
  # - name: CACHE_OWNERS_COUNT
  #   value: "2"
  # - name: CACHE_OWNERS_AUTH_SESSIONS_COUNT
  #   value: "2"
  - name: KEYCLOAK_STATISTICS
    value: "all"
  - name: LAUNCH_JBOSS_IN_BACKGROUND
    value: "no"
  - name: PROXY_ADDRESS_FORWARDING
    value: "true"
{%- if config['keycloak']['postgresAddress'] != 'internal' %}
  - name: DB_VENDOR
    value: "postgres"
  - name: DB_ADDR
    value: "{{ config['keycloak']['postgresAddress'] }}"
  - name: DB_PORT
    value: "{{ config['keycloak']['postgresPort'] }}"
  - name: DB_DATABASE
    value: "{{ config['keycloak']['postgresDatabase'] }}"
  - name: DB_USER
    value: "{{ config['keycloak']['postgresUser'] }}"
  - name: DB_PASSWORD
    value: "{{ config['keycloak']['postgresPassword'] }}"
{%- endif %}
  - name: JGROUPS_DISCOVERY_PROTOCOL
    value: kubernetes.KUBE_PING
  - name: KUBERNETES_NAMESPACE
    valueFrom:
      fieldRef:
        apiVersion: v1
        fieldPath: metadata.namespace
  - name: CACHE_OWNERS_COUNT
    value: "2"
  - name: CACHE_OWNERS_AUTH_SESSIONS_COUNT
    value: "2"
{%- if config['keycloak']['realmImport'] == 'true' %}
  - name: KEYCLOAK_IMPORT
    value: /realm/realm-export.json
{%- endif %}
  - name: KEYCLOAK_USER
    value: "{{ config['keycloak']['adminUser'] }}"
  - name: KEYCLOAK_PASSWORD
    value: "{{ config['keycloak']['adminPassword'] }}"
  - name: JAVA_OPTS
    value: >-
      -XX:+UseContainerSupport
      -XX:MaxRAMPercentage=50.0

# Readiness probe configuration
{%- if config['keycloak']['realmImport'] == 'true' %}
{%-     set readinessProbeRealm = config['keycloak']['realm'] %}
{%- else %}
{%-     set readinessProbeRealm = 'master' %}
{%- endif %}
readinessProbe: |
  httpGet:
    path: /auth/realms/{{ readinessProbeRealm }}
    port: http

# Pod resource requests and limits
resources:
  requests:
    cpu: "{{ config['keycloak']['requests-cpu'] | default(config['general']['requests-cpu'], true) }}"
    memory: "{{ config['keycloak']['requests-memory'] | default(config['general']['requests-memory'], true) }}"
  limits:
    cpu: "{{ config['keycloak']['limits-cpu'] | default(config['general']['limits-cpu'], true) }}"
    memory: "{{ config['keycloak']['limits-memory'] | default(config['general']['limits-memory'], true) }}"

{%- if config['keycloak']['realmImport'] == 'true' %}
# Add additional volumes, e. g. for custom themes
extraVolumes: |
  - name: realm-secret
    secret:
      secretName: realm-secret

# Add additional volumes mounts, e. g. for custom themes
extraVolumeMounts: |
  - name: realm-secret
    mountPath: "/realm/"
    readOnly: true
{%- endif %}

ingress:
  # If `true`, an Ingress is created
  enabled: true
  # The Service port targeted by the Ingress
  servicePort: http
  # Ingress annotations
  {% if config['general']['ingressController'] == 'nginx' %}
  # The name of the Ingress Class associated with this ingress
  ingressClassName: {{ config['general']['ingressClassName'] | default('nginx', true) }}
  annotations:
    nginx.ingress.kubernetes.io/proxy-buffer-size: "16k"
    {%- if config['cert-manager']['enabled'] == "true" %}
    cert-manager.io/cluster-issuer: "{{ config['general']['ingressClusterIssuerName']|default('letsencrypt', true) }}-{{ config['cert-manager']['type'] }}"
    {%- endif %}
    {%- if config['general']['enable-metrics'] == "true" %}
    nginx.ingress.kubernetes.io/server-snippet: |
      location ~* /auth/realms/[^/]+/metrics {
          return 403;
      }
    {%- endif %}
  {% else %}
  annotations: {}
  {% endif %}
  # Additional Ingress labels
  labels: {}
   # List of rules for the Ingress
  rules:
    -
      # Ingress host
      host: "{{ config['keycloak']['hostname'] }}.{{ config['general']['domain'] }}"
      # Paths for the host
      paths:
        - path: /
          pathType: Prefix
  # TLS configuration
  tls:
    - hosts:
        - "{{ config['keycloak']['hostname'] }}.{{ config['general']['domain'] }}"
      {%- if config['cert-manager']['enabled'] == "true" %}
      secretName: tls-{{ config['keycloak']['hostname'] }}
      {%- endif %}

postgresql:
  # If `true`, the Postgresql dependency is enabled
{%- if config['keycloak']['postgresAddress'] == 'internal' %}
  enabled: true
  image:
    tag: 14.5.0
  # PostgreSQL "postgres" admin password
  postgresqlPostgresPassword: "{{ config['keycloak']['postgresAdminPassword'] }}"
  # PostgreSQL User to create
  postgresqlUsername: "{{ config['keycloak']['postgresUser'] }}"
  # PostgreSQL Password for the new user
  postgresqlPassword: "{{ config['keycloak']['postgresPassword'] }}"
  # PostgreSQL Database to create
  postgresqlDatabase: "{{ config['keycloak']['postgresDatabase'] }}"
{%- else %}
  enabled: false
{%- endif %}
  # PostgreSQL network policy configuration
  networkPolicy:
    enabled: false
  persistence:
    {%- if config['general']['demoMode'] == 'true' %}
    enabled: false
    {%- else %} 
    enabled: true
    {%- endif %}

serviceMonitor:
  # If `true`, a ServiceMonitor resource for the prometheus-operator is created
  {%- if config['general']['enable-serviceMonitor'] == "true" %}
  enabled: true
  {%- else %}
  enabled: false
  {%- endif %}

extraServiceMonitor:
  # If `true`, a ServiceMonitor resource for the prometheus-operator is created
  {%- if config['general']['enable-serviceMonitor'] == "true" %}
  enabled: true
  {%- else %}
  enabled: false
  {%- endif %}

autoscaling:
  # If `true`, a autoscaling/v2beta2 HorizontalPodAutoscaler resource is created (requires Kubernetes 1.18 or above)
  # Autoscaling seems to be most reliable when using KUBE_PING service discovery (see README for details)
  # This disables the `replicas` field in the StatefulSet
  enabled: {{ config['keycloak']['autoscalingEnabled']|default(config['general']['autoscalingEnabled'], true) }}
  # Additional HorizontalPodAutoscaler labels
  labels: {}
  # The minimum and maximum number of replicas for the Keycloak StatefulSet
  minReplicas: {{ config['keycloak']['autoscalingMinReplicas'] | default(config['general']['autoscalingMinReplicas'], true) }}
  maxReplicas: {{ config['keycloak']['autoscalingMaxReplicas'] | default(config['general']['autoscalingMaxReplicas'], true) }}
  # The metrics to use for scaling
  metrics:
    - type: Resource
      resource:
        name: cpu
        target:
          type: Utilization
          averageUtilization: {{ config['keycloak']['cpuAverageUtilization'] | default(config['general']['cpuAverageUtilization'], true) }}
