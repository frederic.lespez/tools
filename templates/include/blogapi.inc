# Mongodb parameters to connect to
mongo:
  host: {{ config['mongodb']['mongoName'] }}
  user: {{ config['mongodb']['mongoUsername'] }}
  password: {{ config['mongodb']['mongoPassword'] }}
  rsName: {{ config['mongodb']['mongoRsname'] }}
  database: {{ config['mongodb']['mongoDatabase'] }}
  port: "{{ config['mongodb']['mongoPort'] }}"
