#!/bin/bash

BACKUP_NAMESPACE="backup"
[ ! -e ${BACKUP_NAMESPACE} ] && mkdir -p ${BACKUP_NAMESPACE}

kubectl create namespace ${BACKUP_NAMESPACE} --dry-run=client -o yaml | kubectl apply -f -
kubectl apply -n ${BACKUP_NAMESPACE} -f mongodb-backup-pvc.yaml

kubectl apply -n ${BACKUP_NAMESPACE} -f mongodb-credentials.yaml

kubectl apply -n ${BACKUP_NAMESPACE} -f mongodb-backup-cronjob.yaml
kubectl create -n ${BACKUP_NAMESPACE} job --from=cronjob/backup-mongodb-schedule mongodb-backup
kubectl wait -n ${BACKUP_NAMESPACE} --for=condition=complete --timeout=600s job/mongodb-backup
kubectl -n ${BACKUP_NAMESPACE} logs job/mongodb-backup
#kubectl -n ${BACKUP_NAMESPACE} cp mount-backups-volume:/data/laboite ${BACKUP_NAMESPACE}/laboite
kubectl delete -n ${BACKUP_NAMESPACE} -f mongodb-backup-cronjob.yaml

#keycloak
kubectl apply -n ${BACKUP_NAMESPACE} -f keycloak-backup-pvc.yaml

kubectl apply -n ${BACKUP_NAMESPACE} -f keycloak-credentials.yaml

kubectl apply -n ${BACKUP_NAMESPACE} -f keycloak-backup-cronjob.yaml
kubectl create -n ${BACKUP_NAMESPACE} job --from=cronjob/backup-keycloak-schedule keycloak-backup
kubectl wait -n ${BACKUP_NAMESPACE} --for=condition=complete --timeout=600s job/keycloak-backup
kubectl -n ${BACKUP_NAMESPACE} logs job/keycloak-backup
#kubectl -n ${BACKUP_NAMESPACE} cp mount-backups-volume:/data/laboite ${BACKUP_NAMESPACE}/laboite
kubectl delete -n ${BACKUP_NAMESPACE} -f keycloak-backup-cronjob.yaml

#Minio
kubectl apply -n ${BACKUP_NAMESPACE} -f minio-backup-pvc.yaml

kubectl apply -n ${BACKUP_NAMESPACE} -f minio-credentials.yaml

kubectl apply -n ${BACKUP_NAMESPACE} -f minio-backup-cronjob.yaml
kubectl create -n ${BACKUP_NAMESPACE} job --from=cronjob/backup-minio-schedule minio-backup
kubectl wait -n ${BACKUP_NAMESPACE} --for=condition=complete --timeout=600s job/minio-backup
kubectl -n ${BACKUP_NAMESPACE} logs job/minio-backup
#kubectl -n ${BACKUP_NAMESPACE} cp mount-backups-volume:/data/laboite ${BACKUP_NAMESPACE}/laboite
kubectl delete -n ${BACKUP_NAMESPACE} -f minio-backup-cronjob.yaml
