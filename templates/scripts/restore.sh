#!/bin/bash

BACKUP_NAMESPACE="backup"
kubectl apply -n ${BACKUP_NAMESPACE} -f mongodb-credentials.yaml

kubectl apply -n ${BACKUP_NAMESPACE} -f mongodb-restore-job.yaml
kubectl wait -n  ${BACKUP_NAMESPACE} --for=condition=complete --timeout=600s job/mongodb-restore
kubectl -n ${BACKUP_NAMESPACE} logs job/mongodb-restore
kubectl delete -n ${BACKUP_NAMESPACE} -f mongodb-restore-job.yaml

#keycloak
kubectl scale -n {{ config['general']['namespace'] }} statefulset keycloak --replicas=0
kubectl apply -n ${BACKUP_NAMESPACE} -f keycloak-restore-job.yaml
kubectl wait -n  ${BACKUP_NAMESPACE} --for=condition=complete --timeout=600s job/keycloak-restore
kubectl -n ${BACKUP_NAMESPACE} logs job/keycloak-restore
kubectl delete -n ${BACKUP_NAMESPACE} -f keycloak-restore-job.yaml
kubectl scale -n {{ config['general']['namespace'] }} statefulset keycloak --replicas={{ config['keycloak']['autoscalingMinReplicas'] | default(config['general']['autoscalingMinReplicas'], true) }}

#minio
kubectl apply -n ${BACKUP_NAMESPACE} -f minio-credentials.yaml

kubectl apply -n ${BACKUP_NAMESPACE} -f minio-restore-job.yaml
kubectl wait -n  ${BACKUP_NAMESPACE} --for=condition=complete --timeout=600s job/minio-restore
kubectl -n ${BACKUP_NAMESPACE} logs job/minio-restore
kubectl delete -n ${BACKUP_NAMESPACE} -f minio-restore-job.yaml
